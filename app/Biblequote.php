<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biblequote extends Model
{
	protected $fillable = [
			'biblequote', 'biblebook', 'biblebookln', 'bblbooktop', 'bblbooktopln', 'bibleverse', 'bibleverseln',
	];
	public $timestamps = false;
}
