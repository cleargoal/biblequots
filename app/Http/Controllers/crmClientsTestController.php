<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class crmClientsTestController extends Controller
{
    
    public function checkClients(Request $request) {
        if(!$request->route)
            $this->showClients();
        elseif ($request->route == 'create')
            $this->createClient($request);
        elseif ($request->route == 'update')
            $this->updateClient($request);
        return redirect()->route('crm_test_clients');
    }

    public function createClient (Request $request) {
        $client = new Client;
        $client->fill($request->input())->save();
//        return redirect()->route('crm_test_clients');
    }
    
    public function updateClient (Request $request) {
        $client = Client::find($request->update);
        $client->fill($request->input())->save();
        //        $clients = $this->getClients();
//        return view('crm_test_clients', ['clients' => $clients]);
    }
    
    public function showClients () {
        $clients = Client::orderBy('clients.id')
            ->join('client_types', 'clients.type', 'client_types.id')
            ->select('clients.*', 'client_types.type_name')
            ->get();
        return view('crm_test_clients', ['clients' => $clients]);
    }
    
/*    protected function getClients () {
        $clients = Client::orderBy('clients.id')
            ->join('client_types', 'clients.type', 'client_types.id')
            ->select('clients.*', 'client_types.type_name')
            ->get();
        return $clients;
    }*/
    
}
