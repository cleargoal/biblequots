<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biblequote;
use Illuminate\Support\Facades\Cookie;
// use Illuminate\View\View;

class BibleQuotesController extends Controller
{
	protected $request, $mode, $lastbbqid, $bbquote, $maxdb;
    public function __construct () {
        $this->request = Request::capture();
        $this->mode = $this->request->mode;
        $this->maxdb = config('settings.max_db'); /** BAD solution: just for Known records amount */
	}
    
    public function showBbq () {
        $data = $this->getBbq();
		return view('viewbbq')->with($data); // , ['data'=>$data]
    }
	
	
	public function getBbq () { //
        $bq_id = $this->getNewBbqId();
//		$this->bbquote = Biblequote::find($bq_id);
		$link = config('settings.bible_url') . $this->bbquote['biblebookln'] . '/' . $this->bbquote['bblbooktopln'] . '/' . $this->bbquote['bibleverseln'];
		$from_replace = array(' ', '-');
		$to_replace = array(html_entity_decode('&nbsp;'), html_entity_decode('&#8209;'));
		$anchor = str_replace($from_replace, $to_replace, $this->bbquote['biblebook'].' '.$this->bbquote['bblbooktop'].':'.$this->bbquote['bibleverse']); // &#8209;
		$text = $this->bbquote['biblequote'];

        $pr = ($bq_id == 1) ? 'disabled' : ''; // for 'first' and 'prev' buttons in view
        $nx = ($bq_id == $this->maxdb) ? 'disabled' : '';  // for 'last' and 'next' buttons in view
        
        $data = array(['link' => $link, 'anchor' => $anchor, 'text' => $text, 'pr' => $pr, 'nx' => $nx, 'lastbbqid'=>$this->lastbbqid]);
		return $data;
	}
	
	private function getNewBbqId() {
		try {
		    $this->lastbbqid = Cookie::get('bbqid');
		}
		catch (\Exception $x) {
        }
        
        if (empty($this->lastbbqid)) { // for 1st time for User
            $newbbqid = Biblequote::select('id')->orderBy('id')->first()->id;
//            return $newbbqid;
        }
        else {
            $newbbqid = $this->direction();
        }
//        if (empty($newbbqid))
        
        $minutes = 5270400;
        try {
            Cookie::queue('bbqid', $newbbqid, $minutes);
        }
        catch (\Exception $e) {
        }
        
		return $newbbqid;
	}
	
	protected function direction() {
        switch ($this->mode) {
            case 'first':
                $this->bbquote = Biblequote::orderBy('id')->find(1); // OK
                break;
            case 'prev':
                $this->bbquote = Biblequote::orderBy('id')->find(intval($this->lastbbqid)-1);
                break;
            case 'next':
                $this->bbquote = Biblequote::orderBy('id')->find(intval($this->lastbbqid)+1); // OK
                break;
            case 'last':
                $this->bbquote = Biblequote::orderBy('id')->find($this->maxdb);
                break;
            default:
                $this->bbquote = Biblequote::orderBy('id')->find(intval($this->lastbbqid)+1);
        }
        return $this->bbquote->id;
    }
	
}
