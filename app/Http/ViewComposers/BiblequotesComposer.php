<?php
namespace app\Http\ViewComposers;
use App\Biblequote;
use Illuminate\View\View;
use App\Http\Controllers\BibleQuotesController;

class BiblequotesComposer
{
    /** @var Biblequote  +++ also executes Open graph meta tags  */
	protected $biblequotes;
	
	/** * Create a new Bible quotes composer.
	 * @param  ?UserRepository?  $bbqs * @return void  	 */
	public function __construct(Biblequote $biblequotes)
	{
		// Dependencies automatically resolved by service container...
		$this->biblequotes = $biblequotes;
	}
	
	/*** Bind data to the view.
	 * @param  View  $view * @return void */
	public function compose(View $view)
	{
		$bcc = new BibleQuotesController;
		$data = $bcc->getBbq();
/*		$routename = Route::currentRouteName();
		if ($routename != 'showvslug') {
            $ogdata = $this->ogSetter();
            $view->with(compact('data', 'ogdata'));
        }
        else $view->with(compact('data'));*/
        $view->with(compact('data'));
	}
	
	/** fills open graph variables for all pages exclude Vicard */
	private function ogSetter () {
        $og_title = config('app.name');
        $og_url = 'toolkit-shelley-nautical-outlay';
        $og_image = asset('storage/og-header.jpg');
        $og_description = config('logic.ogdescrip');
        return ['og_title' => $og_title, 'og_url' => $og_url, 'og_image' => $og_image, 'og_description' => $og_description];
    }
	
}
