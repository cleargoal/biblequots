CHDIR = cd ${PWD}/docker
CONTAINERS = nginx mysql

start:
	@echo "Starting containers for laravel project at ${PWD} ..."

	${CHDIR} ; docker-compose up ${CONTAINERS}

.PHONY: start


startd:
	@echo "Starting containers for laravel project at ${PWD} ..."

	${CHDIR} ; docker-compose up -d ${CONTAINERS}

.PHONY: startd


stop:
	@echo "Stopping containers for laravel project at ${PWD}..."
	${CHDIR} ; docker-compose stop ${CONTAINERS}

.PHONY: stop


shell:
	@echo "Entering workspace for laravel project at ${PWD}..."
	${CHDIR} ; docker-compose exec workspace bash

.PHONY: shell


