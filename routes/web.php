<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| */
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get( '/crm_test_clients', 'crmClientsTestController@showClients' )->name( 'crm_test_clients' );
});

Route::get('/register', function (){
   return view('/auth/register');
})->name('register');

Route::get('/login', function (){
    return view('/auth/login');
})->name('login');

Route::get('/logout', function (){
    Auth::logout();
    return view('/auth/login');
})->name('logout');

Route::post('/crm_test_create', 'crmClientsTestController@createClient')->name('crm_test_create');
Route::post('/crm_test_update', 'crmClientsTestController@updateClient')->name('crm_test_update');
Route::post('/crm_test_check', 'crmClientsTestController@checkClients')->name('crm_test_check');

/** ****************************** */

Route::get('/{mode}', 'BibleQuotesController@showBbq')->name('withmode');

/** go through Bible Quotes */
Route::post('/otherBbq', 'BibleQuotesController@showBbq')->name('nextbbqoute');

Route::get('/', function () {
    return view('viewbbq');
});
