CREATE TABLE homestead_bbq.client_types
(
    id int(10) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    type_name varchar(255) NOT NULL
);
INSERT INTO homestead_bbq.client_types (id, type_name) VALUES (1, 'Мужчина');
INSERT INTO homestead_bbq.client_types (id, type_name) VALUES (2, 'Женщина');
INSERT INTO homestead_bbq.client_types (id, type_name) VALUES (3, 'Ребенок');
INSERT INTO homestead_bbq.client_types (id, type_name) VALUES (4, 'Подросток');