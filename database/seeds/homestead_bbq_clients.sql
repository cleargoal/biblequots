CREATE TABLE homestead_bbq.clients
(
    id int(10) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    type int(11) NOT NULL,
    purchase_date date NOT NULL,
    created_at timestamp,
    updated_at timestamp
);
CREATE INDEX clients_name_index ON homestead_bbq.clients (name);
CREATE INDEX clients_type_index ON homestead_bbq.clients (type);
CREATE INDEX clients_purchase_date_index ON homestead_bbq.clients (purchase_date);
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (1, 'First-end', 1, '2018-09-18', '2018-10-21 17:54:42', '2018-10-21 20:30:37');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (2, 'First-st', 1, '2018-09-19', '2018-10-21 17:57:50', '2018-10-21 20:31:06');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (3, 'First', 1, '2018-09-18', '2018-10-21 18:00:38', '2018-10-21 18:00:38');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (4, 'Second', 3, '2018-06-15', '2018-10-21 18:26:06', '2018-10-21 18:26:06');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (5, 'Second', 3, '2018-06-15', '2018-10-21 18:27:37', '2018-10-21 18:27:37');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (6, 'Second', 3, '2018-06-15', '2018-10-21 18:29:10', '2018-10-21 18:29:10');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (7, 'Third', 2, '2018-10-21', '2018-10-21 18:31:26', '2018-10-21 18:31:26');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (8, 'Third', 2, '2018-10-21', '2018-10-21 18:31:59', '2018-10-21 18:31:59');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (9, 'Forth', 4, '2018-08-20', '2018-10-21 18:37:24', '2018-10-21 18:37:24');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (10, 'Next', 4, '2017-09-20', '2018-10-21 18:39:00', '2018-10-21 18:39:00');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (11, 'More...', 2, '2018-11-13', '2018-10-21 18:40:03', '2018-10-21 18:40:03');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (12, 'Renvjljhf', 3, '2018-10-21', '2018-10-21 18:43:15', '2018-10-21 18:43:15');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (13, 'Renvjljhf', 3, '2018-10-21', '2018-10-21 18:44:47', '2018-10-21 18:44:47');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (14, 'Renvjljhf', 3, '2018-10-21', '2018-10-21 18:44:52', '2018-10-21 18:44:52');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (15, 'Momada', 2, '2018-11-20', '2018-10-21 18:52:14', '2018-10-21 18:52:14');
INSERT INTO homestead_bbq.clients (id, name, type, purchase_date, created_at, updated_at) VALUES (16, 'Macumba', 1, '2018-12-13', '2018-10-21 18:54:19', '2018-10-21 18:54:19');