<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
use App\Biblequote;

class BiblequotesSeeder extends Seeder
{
	/**  Run the database seeds.  @return void */
	public function run()
	{
		$reader = Reader::createFromPath(base_path('database/seeds/homestead_biblequotes.csv'));
//		$reader_all = $reader->fetchAll();
		$reader_all = $reader->getRecords();
		// info ($reader_all);

		// $reader_line = $reader->fetchOne();
		// info ($reader_line);

		// создать новый массив - ассоциативный
		$reader_ass = [];

		
		foreach ($reader_all as $key => $val) {
			foreach ($val as $skey => $sval) {
	//			if ($skey == 0) $reader_ass['id'] = $sval;
				if ($skey == 1) $reader_ass['biblequote'] = $sval;
				if ($skey == 2) $reader_ass['biblebook'] = $sval;
				if ($skey == 3) $reader_ass['biblebookln'] = $sval;
				if ($skey == 4) $reader_ass['bblbooktop']= $sval;
				if ($skey == 5) $reader_ass['bblbooktopln'] = $sval;
				if ($skey == 6) $reader_ass['bibleverse'] = $sval;
				if ($skey == 7) $reader_ass['bibleverseln'] = $sval;
			}
			$bbq = Biblequote::create($reader_ass);
		}
		// info ($reader_ass);
	}
}
