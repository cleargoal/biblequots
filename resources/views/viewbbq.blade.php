@extends('layouts.app')

@section('content')
<div class="container">
    <div class = "row" >
        <div class = "col-md-10 col-md-offset-1" >
            <div class = "panel panel-default" >
                <div class = "panel-heading" ><h4 >{{$data[0]['anchor']}}</h4 ></div >
                <div class = "panel-body" >
                    {{--<div class="col-md-7 centered">--}}
                        <p class="biblequote"><p id="bbqtext">{{$data[0]['text']}}</p>
                            <a id="bbqlink" href="{{$data[0]['link']}}" target="_blank"> (<span id="bbqanchor">{{$data[0]['anchor']}}</span>)</a>
{{--                            <span title="Еще цитата" class="nextquote" id="nextquote" data-href="{{ route('nextbbqoute'), ['mode' => 'next'] }}" data-csrf-token="{{ csrf_token()}}">&#9658;</span>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    {{-- debugging --}}
    {{--<div>bbqid -  {{$data[0]['bqid']}}</div>--}}
</div>
@endsection
