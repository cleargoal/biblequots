<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Избранные цитаты из Библии') }}</title>
	{{-- Open graph section --}}
{{--	<meta property="og:title" content="{{$ogdata['og_title']}}" />
	<meta property="og:url" content="http://{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}" />  --}}{{-- million-ru.topearning.space/ --}}{{--
	<meta property="og:image" content="{{$ogdata['og_image']}}" />
	<meta property="og:description" content="{{$ogdata['og_description']}}" />
	<meta property="og:locale" content="ru_UA" />
	<meta property="og:site_name" content="Избранные цитаты из Библии" />--}}
	{{-- Open graph end --}}
	
	<link rel="icon" href="/img/favicon.jpg">
	<link href='https://fonts.googleapis.com/css?family=Comfortaa:400,300,700&subset=cyrillic-ext' rel='stylesheet'
	      type='text/css'>

	<!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<!-- Scripts -->
	<script>
		window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?>
	</script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body id="app-layout">
<div id="app" class="apppage">
	<nav class="navbar navbar-default navbar-static-top">
	<div class="container">
	<div class="navbar-header" style="width: 100%;">

	<!-- Collapsed Hamburger -->
	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
    </button>
    
    
		<span class="sr-only">Включить навигацию</span>
  
        <span class="icon-bar">
            <a href="{{ route('nextbbqoute', ['mode' => 'first']) }}" title="Первая">
                <button type="button" class="btn btn-outline-primary" style="font-size: 200%;" {{$data[0]['pr']}}>&#9650;</button>
            </a>
        </span>
        
        <span class="icon-bar">
            <a href="{{ route('nextbbqoute', ['mode' => 'prev']) }}" title="Предыдущая">
                <button type="button" class="btn btn-outline-primary" style="font-size: 200%;" {{$data[0]['pr']}}>&#9668;</button>
            </a>
        </span>
        
        <span class="icon-bar">
            <a href="{{ route('nextbbqoute', ['mode' => 'next']) }}" title="Следующая">
                <button type="button" class="btn btn-outline-primary" style="font-size: 200%;" {{$data[0]['nx']}}>&#9658;</button>
            </a>
        </span>
        
        <span class="icon-bar">
            <a href="{{ route('nextbbqoute', ['mode' => 'last']) }}" title="Последняя">
                <button type="button" class="btn btn-outline-primary" style="font-size: 200%;" {{$data[0]['nx']}}>&#9660;</button>
            </a>
        </span>

	<!-- Branding Image -->
	<a class="navbar-brand" href="{{ url('/') }}" style=" float: right;">
		{{--{{ config('app.name', 'Цитаты из Библии') }}--}}
        <img src="/img/favicon.jpg" alt="crist" style="width: 150px;">
	</a>
{{--</div>--}}

<div class="collapse navbar-collapse" id="app-navbar-collapse">
	<!-- Left Side Of Navbar -->
	<ul class="nav navbar-nav">
		<li><a href="" title="Первая">&#9650;</a></li>
	</ul>

	<!-- Right Side Of Navbar -->
	<ul class="nav navbar-nav navbar-right">
	</ul>
</div>

	</div>
	</div>
	</nav>
		

	@yield('content')
</div>

<!-- ********** CREDITS ********** -->
<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 centered">
				<p>&copy Избранные цитаты из Библии</p>
			</div>
		</div>
	</div><!--/container -->
</div><!--/C -->

<!-- JavaScripts -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- Scripts -->
<script src="/js/app.js"></script>
</body>
</html>
