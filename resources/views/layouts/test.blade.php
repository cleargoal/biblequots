<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">

	<!-- CSRF Token -->
	{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

	<title>Test of CRM</title>
	
	<link rel="icon" href="/img/t101.png">
	<link href='https://fonts.googleapis.com/css?family=Comfortaa:400,300,700&subset=cyrillic-ext' rel='stylesheet'
	      type='text/css'>

	<!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    

</head>

<body id="app-layout">
<div id="app" class="apppage">
	<nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header" style="width: 100%;">
        
                <span class="icon-bar">
                    @if(Auth::guest())
                    <a href="{{ route('register') }}" title="Register">
                        <button type="button" class="btn btn-outline-primary" style="font-size: 200%;">Register</button>
                    </a>
                    @endif
                </span>
                
                <span class="icon-bar">
                    @if(Auth::guest())
                    <a href="{{ route('crm_test_clients') }}" title="Login">
                        <button type="button" class="btn btn-outline-primary" style="font-size: 200%;">Login</button>
                    </a>
                    @endif
                </span>
                
{{--                <span class="icon-bar">
                    <a href="{{ route('crm_test', ['mode' => 'e']) }}" title="Edit client">
                        <button type="button" class="btn btn-outline-primary" style="font-size: 200%;">Edit client</button>
                    </a>
                </span>
                
                <span class="icon-bar">
                    <a href="{{ route('crm_test', ['mode' => 'c']) }}" title="Create client">
                        <button type="button" class="btn btn-outline-primary" style="font-size: 200%;">Create client</button>
                    </a>
                </span>--}}

                <span class="icon-bar">
                    @if(!Auth::guest())
                    <a href="{{ route('logout') }}" title="Logout">
                        <button type="button" class="btn btn-outline-primary" style="font-size: 200%;">Logout</button>
                    </a>
                    @endif
                </span>
        
            </div>
        </div>
	</nav>

	@yield('content')
</div>

<!-- JavaScripts -->
{{--<script src="/js/jquery.min.js"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- Scripts -->
<script src="/js/test.js"></script>
</body>
</html>
