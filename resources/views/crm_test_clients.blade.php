@extends('layouts.test')

@section('content')
    <style>
        td{padding: 5px 1em;}
    </style>
<div class="container">
    <div class = "row" >
        <div class = "col-md-10 col-md-offset-1" >
            <div class = "panel panel-default" >
                <div class = "panel-heading" ><h4 >Покупатели и покупки</h4 ></div >
                <div class = "panel-body row" >
                    <div class="col-md-10">
                        <table class="table-hover table-responsive table-bordered">
                            <thead class="thead-dark" style="font-weight: 900;">
                                <tr>
                                    <td>ID</td>
                                    <td>Name</td>
                                    <td>Type</td>
                                    <td>Date of purchase</td>
                                    <td>Created</td>
                                    <td>Modified</td>
                                </tr>
                            </thead>
                            @foreach($clients as $key => $client)
                                <tr>
                                    <td>{{$client->id}}</td>
                                    <td>{{$client->name}}</td>
                                    <td>{{$client->type_name}}</td>
                                    <td>{{$client->purchase_date}}</td>
                                    <td>{{$client->created_at}}</td>
                                    <td>{{$client->updated_at}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="col-md-2">
                        @php
                        @endphp

                        {!! Form::open(['id'=>'client-form', 'route'=>'crm_test_check']) !!}
                        <input type="hidden" id="route" name="route">
                        <select name="update" id="update">
                            <option value="0">Изменить</option>
                            @foreach($clients as $key => $client)
                                <option value="{{$client->id}}" data-name="{{$client->name}}" data-type="{{$client->type}}" data-date="{{$client->purchase_date}}">
                                    {{$client->id}}:{{$client->name}}:{{$client->type_name}}:{{$client->purchase_date}}
                                </option>
                            @endforeach
                        </select>
                        @php
                            echo Form::button('Создать', ['id'=>'clear']);
                            echo Form::label('name', 'Имя покупателя');
                            echo Form::text('name');
                            echo Form::label('type', 'Тип покупателя');
                            echo Form::select('type', array(
                            '' => 'Выбрать тип', '1' => 'Мужчина', '2' => 'Женщина', '3' => 'Ребенок', '4' => 'Подросток'));
                            echo Form::label('purchase_date', 'Выбрать дату покупки');
                            echo Form::date('purchase_date', \Carbon\Carbon::now());
                            echo Form::submit('Сохранить!', array('id' => 'sbm', 'disabled'=>'true'));
                        @endphp
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- debugging --}}
    {{--<div>bbqid -  {{$data[0]['bqid']}}</div>--}}
</div>
@endsection
