$(document).ready(function () {

    $('#client-form').on('submit', function(e) {
/*        e.preventDefault();
        alert($('#client-form').attr('action'));*/
    });

    $('#clear').click( function () {
        $("form")[0].reset();
        $('#route').val('create');
        $('#sbm').prop('disabled', false);
        $('#name').focus();
    });

    $('#update').on('change', function () {
        $('#name').val($('#update option:selected').data('name'));
        $('#type').val($('#update option:selected').data('type'));
        $('#purchase_date').val($('#update option:selected').data('date'));
        $('#route').val('update');
        $('#sbm').prop('disabled', false);
        $('#name').focus();
    });

});
